# Pryid #

A prefixed Yid (an ID generator).

[Yid](https://www.npmjs.com/package/yid) gives IDs like `1552446552551-6781164698768` which is a timestamp and some random digits.

A prefixed Yid just gives you a generator to create prefixed versions.

The prefix is copied from Stripe (and other online services) such that IDs of a particular type are prefixed with a
unique set of letters.

## Install ##

Since `pryid` has `yid` as a `peerDependency`, you must install both:

```
$ npm install yid pryid
```

## Synopsis ##

```
const Pryid = require('pryid')

const UserYid    = Pryid('us')
const AccountYid = Pryid('ac')
const ImageYid   = Pryid('im')

console.log(UserYid())    // 'us-1552446552550-0317011366132'
console.log(UserYid())    // 'us-1552446552551-5278733228333'
console.log(UserYid())    // 'us-1552446552551-3728805268183'

console.log(AccountYid()) // 'ac-1552446552551-9146781130693'
console.log(AccountYid()) // 'ac-1552446552551-3762744315899'
console.log(AccountYid()) // 'ac-1552446552551-6781164698768'


console.log(ImageYid())   // 'im-1552446552551-4537592458073'
console.log(ImageYid())   // 'im-1552446552552-1388960494659'
console.log(ImageYid())   // 'im-1552446552552-5167504039127'
```

## Author ##

```
$ npx chilts

   ╒════════════════════════════════════════════════════╕
   │                                                    │
   │   Andrew Chilton (Personal)                        │
   │   -------------------------                        │
   │                                                    │
   │          Email : andychilton@gmail.com             │
   │            Web : https://chilts.org                │
   │        Twitter : https://twitter.com/andychilton   │
   │         GitHub : https://github.com/chilts         │
   │         GitLab : https://gitlab.org/chilts         │
   │                                                    │
   │   Apps Attic Ltd (My Company)                      │
   │   ---------------------------                      │
   │                                                    │
   │          Email : chilts@appsattic.com              │
   │            Web : https://appsattic.com             │
   │        Twitter : https://twitter.com/AppsAttic     │
   │         GitLab : https://gitlab.com/appsattic      │
   │                                                    │
   │   Node.js / npm                                    │
   │   -------------                                    │
   │                                                    │
   │        Profile : https://www.npmjs.com/~chilts     │
   │           Card : $ npx chilts                      │
   │                                                    │
   ╘════════════════════════════════════════════════════╛
```

## License ##

ISC.
