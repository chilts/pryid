const yid = require('yid')

function makePrefixedYidGenerator(prefix) {
  return function() {
    return prefix + '-' + yid()
  }
}

module.exports = makePrefixedYidGenerator
